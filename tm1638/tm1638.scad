
$fs = 0.2;

t = 2;
ww = 11; // height

module scruw() {
   
      difference() {
          cylinder(ww+t+0.5,r=4);
            translate([0, 0, -t-1]) cylinder(ww+t*2+2+0.5,r=1);
      }
  }
  
  module button() {
      r1 = 3.4;
      r2 = 4;
      union() {
          intersection() {
            cylinder(1.5,r=r2);
            cube([r1*2, r2*2, 10], center =true);
          }
          //  translate([0, 0, -t-1]) 
          cylinder(1.5+t+3,r=r1);
      }
  }


module case() {
addTop = 12;
addB = 5;
addL = 0;
//projection()
union() {

   difference() {
       // outer cube
               translate([-t,-t-addB,-t]) cube([76+t*2,50+t*2+addTop,ww+t*2]);

       // inner cube
       translate([0,-addB,0]) cube([76,50+addTop,90]);
       
       // lcd
       m = 1.5; // middle divider width
       lw = 62; // width of two lcds
       lw2 = (lw-m)/2;
       translate([7+addL,           27, -t*2]) cube([lw2,15,90]);
       translate([7+addL + m + lw2, 27, -t*2]) cube([lw2,15,90]);
       
        // buttons
        w = 7.6;
        for (a =[0:7]) {
                   translate([11 + a*w +addL, 4, -t*2]) cylinder(5,r=3.5);

        }

        // leds
        for (a =[0:7]) {
                   translate([11.1 + a*w +addL, 42+4, -t*2]) cylinder(5,r=3);

        }
       
   }
   
         translate([3, 3, -t]) scruw();
         translate([73, 3, -t]) scruw();
         translate([3, 47, -t]) scruw();
         translate([73, 47, -t]) scruw();

   
   }
   

for (a =[0:8]) {
    translate([3 + a*8.7, 65, -t]) button();
}

}


// translate([0,0,-20]) 
case();

module tm1638board() {
    union() {
        cube([77,50,1.5]);
        
    }
}

//tm1638board();